package ru.t1.amsmirnov.taskmanager.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;

public final class ProjectShowByIdResponse extends AbstractProjectResponse {

    public ProjectShowByIdResponse() {
    }

    public ProjectShowByIdResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectShowByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}