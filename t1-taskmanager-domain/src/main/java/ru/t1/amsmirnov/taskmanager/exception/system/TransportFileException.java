package ru.t1.amsmirnov.taskmanager.exception.system;

public final class TransportFileException extends AbstractSystemException {

    public TransportFileException() {
        super("Error! Transport file has problems...");
    }

    public TransportFileException(final String fileName) {
        super("Error! Transport file \"" + fileName + "\" has problems...");
    }

    public TransportFileException(final String fileName, final Throwable cause) {
        super("Error! Transport file \"" + fileName + "\" has problems...", cause);
    }

}
