package ru.t1.amsmirnov.taskmanager.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class DataJsonSaveFasterXMLRequest extends AbstractUserRequest {

    public DataJsonSaveFasterXMLRequest() {
    }

    public DataJsonSaveFasterXMLRequest(@Nullable String token) {
        super(token);
    }

}
